﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FlwCpHub.DataAccess
{
    public interface IMongoDbAccessConfig
    {
        string GetMongoConnectionString();
    }
}
