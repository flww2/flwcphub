﻿


using FlwCpHub.DataAccess.Documents;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlwCpHub.DataAccess
{
    public class PositionDataAccess : IPositionDataAccess
    {
        private const string DB_NAME = "flwcphub";
        private const string COLLECTION_NAME = "positions";

        IMongoCollection<PositionDocument> posCollection;

        public PositionDataAccess(IMongoDbAccessConfig mongoDbAccessConfig)
        {
            var client = new MongoClient(mongoDbAccessConfig.GetMongoConnectionString());
            posCollection = client.GetDatabase(DB_NAME)                
                .GetCollection<PositionDocument>(COLLECTION_NAME);
        }

        public async Task AddPositions(IList<PositionDocument> positions)
        {            
            
            await posCollection.InsertManyAsync(positions);
        }

        public async Task<IList<PositionDocument>> GetPositions()
        {
            var result = await posCollection.FindAsync<PositionDocument>(new FilterDefinitionBuilder<PositionDocument>().Empty);

            return result.ToList();
        }

    }
}
