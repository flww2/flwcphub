﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace FlwCpHub.DataAccess.Documents
{
    public interface IDocument
    {
        ObjectId Id { get; set; }
    }
}
