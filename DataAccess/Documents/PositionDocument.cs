﻿using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace FlwCpHub.DataAccess.Documents
{
    public class PositionDocument : IDocument
    {
        public ObjectId Id { get; set; }

        [BsonElement]
        public long DeviceId { get; set; }

        [BsonElement]
        public string TimeStamp { get; set; }

        [BsonElement]
        public double Lat { get; set; }

        [BsonElement]
        public double Lon { get; set; }        
    }
}
