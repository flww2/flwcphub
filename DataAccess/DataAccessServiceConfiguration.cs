﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace FlwCpHub.DataAccess
{
    public class DataAccessServiceConfiguration
    {
        public static void ConfigureServices(WebHostBuilderContext context, IServiceCollection services)
        {
            services.AddTransient<IPositionDataAccess, PositionDataAccess>();
        }
    }
}
