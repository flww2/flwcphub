﻿using FlwCpHub.DataAccess.Documents;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace FlwCpHub.DataAccess
{
    public interface IPositionDataAccess
    {
        Task AddPositions(IList<PositionDocument> positions);
        Task<IList<PositionDocument>> GetPositions();
    }
}