﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlwCpHub.Converters
{
    public abstract class ConverterBase<TFrom, TTo>
    {
        public abstract TTo Convert(TFrom from);

        public abstract TFrom Convert(TTo from);

        public virtual IList<TTo> Convert(IList<TFrom> from)
        {
            return from.Select(Convert).ToList();
        }

        public virtual IList<TFrom> Convert(IList<TTo> from)
        {
            return from.Select(Convert).ToList();
        }
    }
}
