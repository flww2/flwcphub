﻿using System.Collections.Generic;

namespace FlwCpHub.Converters
{
    public interface IConverter<TFrom, TTo>
    {
        IList<TTo> Convert(IList<TFrom> from);
        IList<TFrom> Convert(IList<TTo> from);
        TTo Convert(TFrom from);
        TFrom Convert(TTo from);
    }
}