﻿using FlwCpHub.DataAccess.Documents;
using FlwCpHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlwCpHub.Converters
{
    public class PositionConverter : ConverterBase<PositionDocument, PositionDTO>, IConverter<PositionDocument, PositionDTO>
    {
        public override PositionDTO Convert(PositionDocument from)
        {
            return new PositionDTO
            {
                DeviceId = from.DeviceId,
                Lat = from.Lat,
                Lon = from.Lon,
                TimeStamp = from.TimeStamp
            };
        }

        public override PositionDocument Convert(PositionDTO from)
        {
            return new PositionDocument
            {
                DeviceId = from.DeviceId,
                Lat = from.Lat,
                Lon = from.Lon,
                TimeStamp = from.TimeStamp
            };
        }
    }
}
