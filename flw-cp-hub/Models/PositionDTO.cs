﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlwCpHub.Models
{
    public class PositionDTO
    {
        public long DeviceId { get; set; }

        public string TimeStamp { get; set; }

        public double Lat { get; set; }

        public double Lon { get; set; }
    }
}
