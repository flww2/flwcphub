﻿using FlwCpHub.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlwCpHub
{
    public class MongoDbAccessConfig : IMongoDbAccessConfig
    {
        public string GetMongoConnectionString()
        {
            return "mongodb://mongodb:27017";
        }
    }
}
