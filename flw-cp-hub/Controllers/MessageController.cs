﻿using FlwCpHub.Converters;
using FlwCpHub.DataAccess;
using FlwCpHub.DataAccess.Documents;
using FlwCpHub.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlwCpHub.Controllers
{
    [Route("api/positions/")]
    public class MessageController : Controller
    {
        private readonly IPositionDataAccess positionDataAccess;
        private readonly IConverter<PositionDocument, PositionDTO> converter;

        public MessageController(
            IPositionDataAccess positionDataAccess,
            IConverter<PositionDocument, PositionDTO> converter)
        {
            this.positionDataAccess = positionDataAccess;
            this.converter = converter;
        }

        [HttpPost()]
        public async Task AddPositions(
            [FromBody] IList<PositionDTO> positions)
        {
            await positionDataAccess.AddPositions(converter.Convert(positions));
        }

        [HttpGet]
        public async Task<IList<PositionDTO>> GetPositions()
        {
            return converter.Convert(await positionDataAccess.GetPositions());
        }

    }
}
