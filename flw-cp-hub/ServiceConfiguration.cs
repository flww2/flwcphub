﻿using FlwCpHub.Converters;
using FlwCpHub.DataAccess;
using FlwCpHub.DataAccess.Documents;
using FlwCpHub.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlwCpHub
{
    public static class ServiceConfiguration
    {
        public static void ConfigureServices(WebHostBuilderContext context, IServiceCollection services)
        {
            services.AddMvc();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "¨FLW CP HUB", Version = "v1" });
            });

            

            services.AddTransient<IConverter<PositionDocument, PositionDTO>, PositionConverter>();
            services.AddSingleton<IMongoDbAccessConfig, MongoDbAccessConfig>();
            services.AddSingleton<SignalRConnector.SignalRConnector>();
        }
    }
}
