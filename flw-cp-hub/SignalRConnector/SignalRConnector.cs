﻿using FlwCpHub.DataAccess;
using Microsoft.AspNet.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlwCpHub.SignalRConnector
{
    public class SignalRConnector
    {
        private readonly IPositionDataAccess positionDataAccess;

        public SignalRConnector(
            IPositionDataAccess positionDataAccess,
            IMongoDbAccessConfig mongoDbAccessConfig)
        {
            this.positionDataAccess = positionDataAccess;
        }

        public async Task Connect()
        {

            

            var hubConnection = new HubConnection("http://172.31.16.1/FleetwareWebAPI/signalr/");
            hubConnection.Headers.Add("Authorization", "bearer JJxYB5XPWEZfph4KtsnQ11jWuEzeB3uVsivW3InkPCz9NMWkkRXVaFf66aq9NhGF5QajTmcpLG3c_iJs-ruxqby56QTS5TAe95kqqatcCxhcwWqogURtKzfEz52xJyWddd3gVftJPpFma9HQa9oK92ua2IiUfpHPTB4QUXwy9FymPyO2SPByMU3xdMHlMHK45q769SPz4cZj3JPbQNqLTkWd_ZZ7rix_4uc842dpcLRRA9Q893_NKg8-_CIIqtcZT-2IdfXn-sK5aGkf8inNDR0kxJHivOvnwlZRYDJ2hpq0-SyXLQnAVp5sFs1SCFFRfTDNBmG9R14-IYXN9juRCQxWTyy1JQW4BusC1BqMGKNCksXzOQyqhnW8CIaU9SaQWEceGBXUIfYb52l9dBt_OQLF754S31ORsp7ojIadFOiOVn4hXASexAfKtbC786AGaAnJVm-7UX6YKbxP0YEg8MMfKgL5Y7pUK02EmjkZ-JM");
            IHubProxy stockTickerHubProxy = hubConnection.CreateHubProxy("positionHub");            
            stockTickerHubProxy.On<IList<VehicleOnlineDataDTO>>("Positions", async stock =>
            {
                await positionDataAccess.AddPositions(stock.Select(s => new DataAccess.Documents.PositionDocument
                {
                    DeviceId = s.ObjectId,
                    Lat = s.Position.Latitude,
                    Lon = s.Position.Longitude,
                    TimeStamp = s.Time.ToString()
                }).ToList());
            });

            await hubConnection.Start();
        }


    }

    public class VehicleOnlineDataDTO
    {

        public long ObjectId { get; set; }


        public int? Azimuth
        {
            get;
            set;
        }




        public double? Speed { get; set; }

        public GPSPoint Position
        {
            get;
            set;
        }

        public long PositionId { get; set; }




        public bool IsEngineOn { get; set; }

        public DateTime Time { get; set; }

        public IList<IOnlineEventDTO> Events { get; set; }


    }

    public interface IOnlineEventDTO
    {
        int EventType { get; set; }
    }
    public class InputEventDTO : IOnlineEventDTO
    {
        public int EventType { get; set; }

        public int InputNumber { get; set; }
    }

    public class GPSPoint
    {


        /// <summary>
        /// Zemepisna sirka v WGS84 systemu (stupne)
        /// </summary>
        public double Latitude { get; set; }

        /// <summary>
        /// Zemepisna delka v WGS84 systemu (stupne)
        /// </summary>
        public double Longitude { get; set; }



    }
}
