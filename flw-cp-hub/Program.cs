﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using FlwCpHub.DataAccess;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using Microsoft.Extensions.Logging;
//using System.ServiceProcess;

namespace FlwCpHub
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var webHost = BuildWebHost(args);


            webHost.Services.GetService<SignalRConnector.SignalRConnector>().Connect().Wait();



            webHost.Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost

            .CreateDefaultBuilder(args)


            .ConfigureServices(DataAccessServiceConfiguration.ConfigureServices)
            .ConfigureServices(ServiceConfiguration.ConfigureServices)
            .Configure(Startup.Configure)
            .Build();
    }  
}
